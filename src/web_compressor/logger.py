"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

import logging

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
