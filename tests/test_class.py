"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

import pathlib
import re

from web_compressor import WebCompressor
from web_compressor.utils import append2file

from .file_system import FileSystem


def test_collect_html(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.collect_html'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)

    # Assert result
    assert obj.collect_html() == [filesys.get_html_home(), filesys.get_html_about()]


def test_collect_html_blocked_files(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.collect_html' using blocked files

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)

    # Run function
    obj.block_list = ["index.html"]

    # Assert result
    assert obj.collect_html() == [filesys.get_html_about()]


def test_collect_assets(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.collect_assets'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)

    # Assert result
    assert obj.collect_assets() == [
        (filesys.get_favicon(), "image/vnd.microsoft.icon"),
        (filesys.get_xml(), "application/xml"),
        (filesys.get_pubkey(), "application/pgp-keys"),
        (filesys.get_json(), "application/json"),
        (filesys.get_js(), "text/javascript"),
        (filesys.get_png(), "image/png"),
        (filesys.get_jpg(), "image/jpeg"),
        (filesys.get_css(), "text/css"),
    ]


def test_collect_assets_blocked_files(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.collect_assets' using blocked files

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)

    # Run function
    obj.block_list = ["styles.css", "scripts.js"]

    # Assert result
    assert obj.collect_assets() == [
        (filesys.get_favicon(), "image/vnd.microsoft.icon"),
        (filesys.get_xml(), "application/xml"),
        (filesys.get_pubkey(), "application/pgp-keys"),
        (filesys.get_json(), "application/json"),
        (filesys.get_png(), "image/png"),
        (filesys.get_jpg(), "image/jpeg"),
    ]


def test_minify_assets(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.minify_assets'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)
    css_file = filesys.get_css()
    js_file = filesys.get_js()
    json_file = filesys.get_json()
    xml_file = filesys.get_xml()

    # Run function
    obj.minify_assets()

    # Assert results
    # (1) Minification
    # (a) CSS
    assert not css_file.exists()
    css_min_file = append2file(css_file, "min")
    assert css_min_file.exists()
    assert css_min_file.read_text("utf-8") == "body{background:#000;color:#000}"

    # (b) JS
    assert not js_file.exists()
    js_min_file = append2file(js_file, "min")
    assert js_min_file.exists()
    assert js_min_file.read_text("utf-8") == 'console.log("Hello World!")'

    # (c) JSON
    assert not json_file.exists()
    json_min_file = append2file(json_file, "min")
    assert json_min_file.exists()
    assert (
        json_min_file.read_text("utf-8")
        == '{"styles.css":"styles.min.abc.css","scripts.js":"scripts.min.123.js"}'
    )

    # (d) XML
    assert not xml_file.exists()
    xml_min_file = append2file(xml_file, "min")
    assert xml_min_file.exists()
    assert (
        xml_min_file.read_text("utf-8")
        == '<?xml version="1.0" encoding="UTF-8"?>'
        + '<Root param="value">Hello World!</Root>'
    )

    # (2) Ignored files ..
    # (a) .. inside '.well-known'
    assert filesys.get_well_known_json().exists()

    # (b) .. favicon
    assert filesys.get_favicon().exists()

    # (3) Updated references
    html = filesys.get_html_home().read_text("utf-8")

    for text in ["db.min.xml", "scripts.min.js", "styles.min.css"]:
        assert text in html


def test_minify_html(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.minify_html'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)

    # Run function
    obj.minify_html()

    # Assert results
    for html_file in [filesys.get_html_home(), filesys.get_html_about()]:
        assert "<head>" not in html_file.read_text("utf-8")


def test_optimize_images(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.optimize_images'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)
    jpg_file = filesys.get_jpg()
    jpg_size = jpg_file.stat().st_size
    png_file = filesys.get_png()
    png_size = png_file.stat().st_size

    # Run function
    obj.optimize_images()

    # Assert results
    # (1) JPEG
    jpg_min_file = filesys.get_jpg_min()
    assert jpg_min_file.exists()
    assert jpg_size > jpg_min_file.stat().st_size
    assert not jpg_file.exists()

    # (2) PNG
    png_min_file = filesys.get_png_min()
    assert png_min_file.exists()
    assert png_size > png_min_file.stat().st_size
    assert not png_file.exists()


def test_convert_images(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.convert_images'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)
    jpg_file = filesys.get_jpg()
    png_file = filesys.get_png()

    # Run function
    obj.convert_images()

    # Assert results
    # (1) JPEG
    assert jpg_file.exists()
    assert jpg_file.with_suffix(".webp").exists()
    assert jpg_file.with_suffix(".avif").exists()

    # (2) PNG
    assert png_file.exists()
    assert png_file.with_suffix(".webp").exists()
    assert png_file.with_suffix(".avif").exists()


def test_hash_assets(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.hash_assets'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)
    css_file = filesys.get_css()
    js_file = filesys.get_js()
    json_file = filesys.get_json()
    xml_file = filesys.get_xml()
    jpg_file = filesys.get_jpg()
    png_file = filesys.get_png()
    ico_file = filesys.get_favicon()

    # Run function
    obj.hash_assets()

    # Assert results
    # (1) Hashing
    # (a) CSS
    assert not css_file.exists()
    assert append2file(css_file, "f0c0a1424d").exists()

    # (b) JS
    assert not js_file.exists()
    assert append2file(js_file, "ee18f6cfdd").exists()

    # (c) JSON
    assert not json_file.exists()
    assert append2file(json_file, "031c3dddc6").exists()

    # (d) XML
    assert not xml_file.exists()
    assert append2file(xml_file, "655cb53bb2").exists()

    # (e) JPEG
    assert not jpg_file.exists()
    assert append2file(jpg_file, "d788bc16a1").exists()

    # (f) PNG
    assert not png_file.exists()
    assert append2file(png_file, "5f42d0ddeb").exists()

    # (g) Favicon
    assert not ico_file.exists()
    assert append2file(ico_file, "786a02f742").exists()

    # (2) Ignored files ..
    # (a) .. inside '.well-known'
    assert filesys.get_well_known_json().exists()

    # (b) HTML files
    assert filesys.get_html_home().exists()
    assert filesys.get_html_about().exists()

    # (c) Other files
    assert filesys.get_pubkey().exists()


def test_generate_sri(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.generate_sri'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)

    # Run function
    obj.generate_sri("sha256")

    # Assert results
    html = filesys.get_html_home().read_text("utf-8")

    for text in [
        "sha256-5fNSOzhunmSZW/Y1rVY5t29rN2d0G+7gy4x+9RUqwzQ=",
        "sha256-ryK03Ll/PXm/ivCoDX4w8LElo0wVuiEiYn9gAMIyzMA=",
    ]:
        assert text in html


def test_generate_csp(tmp_path: pathlib.Path) -> None:
    """
    Tests 'WebCompressor.generate_csp'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = WebCompressor(tmp_path)

    # Run function
    obj.generate_csp(digest="sha256")

    # Assert results
    html = filesys.get_html_home().read_text("utf-8")

    for text in [
        "object-src none",
        "script-src",
        "'strict-dynamic'",
        "style-src",
        "base-uri none",
        'http-equiv="content-security-policy"',
    ]:
        assert text in html

    assert re.search(r"nonce-\w{32}", html) is not None
