"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

import pathlib

from click.testing import CliRunner

from web_compressor.cli import cli

from .file_system import FileSystem


def test_cli(tmp_path: pathlib.Path) -> None:
    """
    Tests CLI interface

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    FileSystem(tmp_path)
    runner = CliRunner()

    # Run function
    result = runner.invoke(cli, str(tmp_path))

    # Assert result
    assert result.exit_code == 0
