"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

import pathlib

from web_compressor.utils import (
    append2file,
    dump_toml,
    get_mime,
    is_loaded,
    load_toml,
    read_file,
)

from .file_system import FileSystem


def test_append2file(tmp_path: pathlib.Path) -> None:
    """
    Tests 'append2file'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    file = tmp_path / "file.ext"

    # Assert result
    assert append2file(file, "min").name == "file.min.ext"


def test_get_mime() -> None:
    """
    Tests 'get_mime'

    :return: None
    """

    # Assert results
    # (1) Invalid MIME type
    assert get_mime("in.valid") is None

    # (2) Valid MIME type
    assert get_mime("valid.xyz") == "chemical/x-xyz"


def test_is_loaded() -> None:
    """
    Tests 'is_loaded'

    :return: None
    """

    # Assert results
    # (1) Valid module (from 'stdlib')
    assert is_loaded("sys")

    # (2) Invalid module
    assert not is_loaded("xyz_module")

    # (3) Multiple modules
    assert is_loaded(["sys", "zlib"])


def test_read_file(tmp_path: pathlib.Path) -> None:
    """
    Tests 'read_file'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    file = tmp_path / "file.ext"
    file.write_text("This is a simple test.")

    # Run function
    data = read_file(file)

    # Assert results
    # (1) Data type
    assert isinstance(data, bytes)

    # (2) File contents
    assert data == b"This is a simple test."


# TOML functions


def test_dump_toml(tmp_path: pathlib.Path) -> None:
    """
    Tests 'dump_toml'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    config_file = tmp_path / "config.toml"

    # Assert results
    assert dump_toml({"key": "value"}, config_file) is None
    assert dump_toml({"key": "value"}, str(config_file)) is None


def test_load_toml(tmp_path: pathlib.Path) -> None:
    """
    Tests 'load_toml'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    FileSystem(tmp_path)
    config_file = tmp_path / "data" / "data.toml"
    assert config_file.exists()

    # Assert result
    assert load_toml(config_file) == {
        "bool": True,
        "float": 1.2,
        "int": 10000,
        "str": "lol",
    }
