"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

import pathlib
import shutil


class FileSystem:
    """
    Handles temporary files for testing
    """

    def __init__(self, base_dir: pathlib.Path) -> None:
        """
        :return: None
        """

        self.base_dir = base_dir

        self.clone_structure()

    def clone_structure(self) -> None:
        """
        Clones directory structure of 'fixture'

        :return: None
        """

        # Determine directory to clone
        fixtures = pathlib.Path(__file__).parent / "fixtures"

        for file in fixtures.glob("*"):
            if file.is_file():
                shutil.copy(file, self.base_dir / file.name)

            if file.is_dir():
                shutil.copytree(file, self.base_dir / file.name)

    def get_css(self) -> pathlib.Path:
        """
        Retrieves 'CSS' file

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "css" / "styles.css"

    def get_css_min(self) -> pathlib.Path:
        """
        Retrieves 'CSS' file (minified)

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "css" / "styles.min.css"

    def get_js(self) -> pathlib.Path:
        """
        Retrieves 'JS' file

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "js" / "scripts.js"

    def get_js_min(self) -> pathlib.Path:
        """
        Retrieves 'JS' file (minified)

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "js" / "scripts.min.js"

    def get_json(self) -> pathlib.Path:
        """
        Retrieves 'JSON' file

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "manifest.json"

    def get_json_min(self) -> pathlib.Path:
        """
        Retrieves 'JSON' file (minified)

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "manifest.min.json"

    def get_jpg(self) -> pathlib.Path:
        """
        Retrieves 'JPEG' file

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "img" / "bled.jpg"

    def get_jpg_min(self) -> pathlib.Path:
        """
        Retrieves 'JPEG' file (minified)

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "img" / "bled.min.jpg"

    def get_png(self) -> pathlib.Path:
        """
        Retrieves 'PNG' file

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "img" / "lesepeter.png"

    def get_png_min(self) -> pathlib.Path:
        """
        Retrieves 'PNG' file (minified)

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "img" / "lesepeter.min.png"

    def get_xml(self) -> pathlib.Path:
        """
        Retrieves XML file

        :return: pathlib.Path
        """

        return self.base_dir / "data" / "db.xml"

    def get_xml_min(self) -> pathlib.Path:
        """
        Retrieves XML file (minified)

        :return: pathlib.Path
        """

        return self.base_dir / "data" / "db.min.xml"

    def get_html_home(self) -> pathlib.Path:
        """
        Retrieves 'home' HTML file

        :return: pathlib.Path
        """

        return self.base_dir / "index.html"

    def get_html_about(self) -> pathlib.Path:
        """
        Retrieves 'about' HTML file

        :return: pathlib.Path
        """

        return self.base_dir / "about" / "index.htm"

    def get_well_known_json(self) -> pathlib.Path:
        """
        Retrieves 'assetlinks' below '.well-known'

        :return: pathlib.Path
        """

        return self.base_dir / ".well-known" / "assetlinks.json"

    def get_favicon(self) -> pathlib.Path:
        """
        Retrieves 'favicon'

        :return: pathlib.Path
        """

        return self.base_dir / "favicon.ico"

    def get_pubkey(self) -> pathlib.Path:
        """
        Retrieves 'pubkey' PGP keyfile

        :return: pathlib.Path
        """

        return self.base_dir / "assets" / "pubkey.asc"
