"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

from pathlib import Path

from web_compressor.cli.config import Config


def test_init(tmp_path: Path) -> None:
    """
    Tests 'Config.init'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    config_file = tmp_path / "config_path.toml"
    assert not config_file.exists()

    # Run function
    Config(config_file)

    # Assert result
    assert config_file.exists()

    # Setup
    config_file = str(tmp_path / "config_str.toml")
    assert not Path(config_file).exists()

    # Run function
    Config(config_file)

    # Assert result
    assert Path(config_file).exists()


def test_get(tmp_path: Path) -> None:
    """
    Tests 'Config.get'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    config_file = tmp_path / "config.toml"
    obj = Config(config_file)

    # Assert results
    # (1) Single value
    assert isinstance(obj.get("hashing"), dict)

    # (2) Single value & fallback
    assert obj.get("invalid", "value") == "value"

    # (3) Nested value
    assert isinstance(obj.get("hashing.sri"), dict)

    # (4) Nested value & fallback
    assert obj.get("invalid.key", "value") == "value"
