"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

# pylint: disable=duplicate-code

import pathlib

from web_compressor.handlers import Hasher

from ..file_system import FileSystem


def test_init() -> None:
    """
    Tests 'Hasher.init'

    :return: None
    """

    # Setup
    obj = Hasher()

    # Assert result
    assert isinstance(obj, Hasher)


def test_is_ready():
    """
    Tests 'Hasher.is_ready'

    :return: None
    """

    # Setup
    obj = Hasher()

    # Assert result
    assert obj.is_ready()


def test_validate():
    """
    Tests 'Hasher.validate'

    :return: None
    """

    # Setup
    obj = Hasher()

    # Assert results
    assert not obj.validate("text/plain")

    for mime_type in [
        # CSS
        "text/css",
        # Fonts
        "font/ttf",
        "font/woff",
        "font/woff2",
        # JS
        "text/javascript",
        "application/javascript",
        "application/ecmascript",
        # JSON
        "application/json",
        "application/ld+json",
        # Images
        "image/jpeg",
        "image/png",
        # XML
        "application/xml",
        "application/atom+xml",
        "application/rss+xml",
    ]:
        assert obj.validate(mime_type)


def test_hash_file(tmp_path: pathlib.Path):
    """
    Tests 'Hasher.hash_file'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = Hasher()
    css_file = filesys.get_css()
    jpg_file = filesys.get_jpg()

    # Run functions
    hashed_css = obj.hash_file(css_file)
    hashed_jpg = obj.hash_file(jpg_file)

    # Assert results
    # (1) CSS (= text)
    assert hashed_css.exists()
    assert f".f0c0a1424d{hashed_css.suffix}" in hashed_css.name
    assert not css_file.exists()

    # (2) JPEG (= binary)
    assert hashed_jpg.exists()
    assert f".d788bc16a1{hashed_jpg.suffix}" in hashed_jpg.name
    assert not jpg_file.exists()


def test_get_file_hash(tmp_path: pathlib.Path):
    """
    Tests 'Hasher.get_file_hash'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = Hasher()

    # Assert results
    # (1) CSS (= text)
    assert obj.get_file_hash(filesys.get_css()) == (
        "f0c0a1424d7f0d086e0e8e0e7fe3907f8e607cab63438bb6eb2eda699003ea8a3"
        + "071aab561f31cb6784cc594b5cf2bcaf97666949331a769915c7d43d70edc1f"
    )

    # (2) JPEG (= binary)
    assert obj.get_file_hash(filesys.get_jpg()) == (
        "d788bc16a185829b116890655f542cdea2a50696bb989687b3dbf360173da976a"
        + "74b57bc242cc87f4071195c5dde81b91d7322786f9220f7661e0727344ff36e"
    )


def test_get_sri_value(tmp_path: pathlib.Path):
    """
    Tests 'Hasher.get_sri_value'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = Hasher()
    css_file = filesys.get_css()
    jpg_file = filesys.get_jpg()

    # Assert results
    # (1) CSS (= text)
    assert obj.get_sri_value(css_file) == (
        "sha512-3QeKIVH4k9jg4yBwnyRq5JZNpiUTcsUF7fzHgLMyYr"
        + "uDVPYASh9X5osL1bnERABTeSEwD8Gy/E02uPt+GnsyEw=="
    )

    assert (
        obj.get_sri_value(css_file, "sha256")
        == "sha256-5fNSOzhunmSZW/Y1rVY5t29rN2d0G+7gy4x+9RUqwzQ="
    )

    assert obj.get_sri_value(css_file, "sha384") == (
        "sha384-8r4WvqT0D6h//d8rYUD96F97ZvkBexY9AWQzr5tM+rhHoiGAZm8N8m8rCEKCNYPG"
    )

    assert obj.get_sri_value(css_file, "sha512") == (
        "sha512-3QeKIVH4k9jg4yBwnyRq5JZNpiUTcsUF7fzHgLMyYr"
        + "uDVPYASh9X5osL1bnERABTeSEwD8Gy/E02uPt+GnsyEw=="
    )

    # (2) JPEG (= binary)
    assert obj.get_sri_value(jpg_file) == (
        "sha512-AiO38txsgmxJjwI5xNsmiJEIh5MjTVrJqw7TXfVLV"
        + "p+UrWGs8a98qjCO1kFf8iUnF26+hXmPPg8MNc4mSgaT6w=="
    )

    assert obj.get_sri_value(jpg_file, "sha256") == (
        "sha256-NEFugHBYE0XeRGRBWvjhqO+QvvR7JsykBgbmndVOgE0="
    )

    assert obj.get_sri_value(jpg_file, "sha384") == (
        "sha384-c/YJh4T4E/lLJuKdckKCTXx7VA0pnjscx00kJUPMBefCRds9KYH8IVUs7iihVirS"
    )

    assert obj.get_sri_value(jpg_file, "sha512") == (
        "sha512-AiO38txsgmxJjwI5xNsmiJEIh5MjTVrJqw7TXfVLV"
        + "p+UrWGs8a98qjCO1kFf8iUnF26+hXmPPg8MNc4mSgaT6w=="
    )


def test_get_hashed_nonce():
    """
    Tests 'Hasher.get_hashed_nonce'

    :return: None
    """

    # Setup
    obj = Hasher()

    # Assert results
    assert (
        obj.get_hashed_nonce("Hello World!")
        == "sha512-hhhE1nBOhXP+w02WfiC8/vPUJM9IvgTm3AjyvVjH"
        + "KXQzcQFerYkcw88cnTS0kmS1EHUbH/nlN5N7xGtdb/TsyA=="
    )

    assert (
        obj.get_hashed_nonce("Hello World!", "sha256")
        == "sha256-f4OxZX/x/FO5LcGBSKHWXfwtSx+j1ncoSt3SABJtkGk="
    )

    assert (
        obj.get_hashed_nonce("Hello World!", "sha384")
        == "sha384-v9dsDrvQBv7lg0EFR8GIewKSvnbVgtlsJC0qeScj4/1v0GH51c/RO4+WE1jmrbpK"
    )

    assert (
        obj.get_hashed_nonce("Hello World!", "sha512")
        == "sha512-hhhE1nBOhXP+w02WfiC8/vPUJM9IvgTm3AjyvVjH"
        + "KXQzcQFerYkcw88cnTS0kmS1EHUbH/nlN5N7xGtdb/TsyA=="
    )
