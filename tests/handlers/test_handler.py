"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

import re

from web_compressor.handlers import Handler


class TestClass(Handler):
    """
    Subclasses 'Handler' for testing 'validate'
    """

    def is_ready(self):
        """
        Implements abstract method 'is_ready'
        """

        return True


def test_validate():
    """
    Tests 'Handler.validate'
    """

    # Setup
    obj = TestClass()

    # Assert results
    # (1) No ruleset
    assert obj.validate("text/plain")

    # (2) Empty list
    obj.ruleset = []
    assert obj.validate("text/plain")

    # (3) List
    obj.ruleset = ["text/html"]
    assert obj.validate("text/html")
    assert not obj.validate("text/plain")

    # (4) Regular expression
    obj.ruleset = re.compile(r"text\/html")
    assert obj.validate("text/html")
    assert not obj.validate("text/plain")
