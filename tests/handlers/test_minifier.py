"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

# pylint: disable=duplicate-code

import pathlib

from web_compressor.handlers import Minifier

from ..file_system import FileSystem


def test_init() -> None:
    """
    Tests 'Minifier.init'

    :return: None
    """

    # Setup
    obj = Minifier()

    # Assert result
    assert isinstance(obj, Minifier)


def test_is_ready():
    """
    Tests 'Minifier.is_ready'

    :return: None
    """

    # Setup
    obj = Minifier()

    # Assert result
    assert obj.is_ready()


def test_validate():
    """
    Tests 'Minifier.validate'

    :return: None
    """

    # Setup
    obj = Minifier()

    # Assert results
    assert not obj.validate("text/plain")

    for mime_type in [
        # CSS
        "text/css",
        # HTML
        "text/html",
        # JS
        "text/javascript",
        "application/javascript",
        "application/ecmascript",
        # JSON
        "application/json",
        "application/ld+json",
        # SVG
        "image/svg+xml",
        # XML
        "application/xml",
        "application/atom+xml",
        "application/rss+xml",
    ]:
        assert obj.validate(mime_type)


def test_minify_file(tmp_path: pathlib.Path):
    """
    Tests 'Minifier.minify_file'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = Minifier()

    for file in [
        filesys.get_css(),
        filesys.get_js(),
        filesys.get_html_home(),
        filesys.get_json(),
        filesys.get_xml(),
    ]:
        # Run function
        file_size = file.stat().st_size
        minified = obj.minify_file(file)

        # Assert results
        assert minified.exists()
        assert file_size > minified.stat().st_size
        assert not file.exists()


def test_minify_string(tmp_path: pathlib.Path):
    """
    Tests 'Minifier.minify_string'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = Minifier()

    # Assert results
    # (1) CSS
    assert (
        obj.minify_string(filesys.get_css().read_text("utf-8"), "text/css")
        == "body{background:#000;color:#000}"
    )

    # (2) HTML
    for html_file in [filesys.get_html_home(), filesys.get_html_about()]:
        assert "<head>" not in obj.minify_string(
            html_file.read_text("utf-8"), "text/html"
        )

    # (3) JS
    assert (
        obj.minify_string(filesys.get_js().read_text("utf-8"), "application/javascript")
        == 'console.log("Hello World!")'
    )

    # (4) JSON
    assert (
        obj.minify_string(
            filesys.get_json().read_text("utf-8"),
            "application/json",
        )
        == '{"styles.css":"styles.min.abc.css","scripts.js":"scripts.min.123.js"}'
    )

    # (5) XML
    assert (
        obj.minify_string(filesys.get_xml().read_text("utf-8"), "application/xml")
        == '<?xml version="1.0" encoding="UTF-8"?>'
        + '<Root param="value">Hello World!</Root>'
    )
