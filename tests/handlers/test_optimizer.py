"""
This module is part of the 'web-compressor' package,
which is released under GPL-3.0-only license.
"""

import pathlib

from web_compressor.handlers import Optimizer

from ..file_system import FileSystem


def test_init() -> None:
    """
    Tests 'Optimizer.init'

    :return: None
    """

    # Setup
    obj = Optimizer()

    # Assert result
    assert isinstance(obj, Optimizer)


def test_is_ready():
    """
    Tests 'Optimizer.is_ready'

    :return: None
    """

    # Setup
    obj = Optimizer()

    # Assert result
    assert obj.is_ready()


def test_validate():
    """
    Tests 'Optimizer.validate'

    :return: None
    """

    # Setup
    obj = Optimizer()

    # Assert results
    assert not obj.validate("text/plain")

    for mime_type in ["image/jpeg", "image/png"]:
        assert obj.validate(mime_type)


def test_optimize_image(tmp_path: pathlib.Path):
    """
    Tests 'Optimizer.optimize_image'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = Optimizer()

    for file in [filesys.get_jpg(), filesys.get_png()]:
        # Run function
        file_size = file.stat().st_size
        optimized = obj.optimize_image(file)

        # Assert results
        assert optimized.exists()
        assert file_size > optimized.stat().st_size
        assert not file.exists()


def test_convert_image(tmp_path: pathlib.Path):
    """
    Tests 'Optimizer.convert_image'

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    filesys = FileSystem(tmp_path)
    obj = Optimizer()

    for file in [filesys.get_jpg(), filesys.get_png()]:
        # Run function
        avif, webp = obj.convert_image(file)

        # Assert results
        assert file.exists()
        assert avif.exists()
        assert webp.exists()
